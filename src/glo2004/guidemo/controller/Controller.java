package glo2004.guidemo.controller;

import glo2004.guidemo.model.Member;
import glo2004.guidemo.model.MembershipType;
import glo2004.guidemo.model.SportClub;
import glo2004.guidemo.view.MainPanel;

import java.util.List;

public class Controller {
	private final SportClub model;
	private final MainPanel view;

	public Controller(SportClub model, MainPanel view) {
		this.model = model;
		this.view = view;
	}

	public List<Member> getMembers() {
		return model.getMembers();
	}

	public boolean wasUpdated(Member member) {
		Member found = model.getMember(member.getID());
		return !found.equals(member);
	}

	public void processAddMember() {
		model.addMember("<First Name>", "<Last Name>", MembershipType.DAY);
		view.updateListContents();
		view.selectNewMember();
		view.setStatusBar("Member added! Please edit the member's information.");
	}

	public void updateMember(int id, String firstName, String lastName, MembershipType selected) {
		model.updateMember(id, firstName, lastName, selected);
		view.updateListContents();
		view.selectMember(model.getMember(id));
		view.setStatusBar("Member updated!");
	}

	public void updateViewToMatchModel() {
		view.updateListContents();
	}
}
