package glo2004.guidemo.model;

import java.util.Calendar;

public class AnytimeMember extends AbstractMember {
	public AnytimeMember(int id, String firstName, String lastName) {
		super(id, firstName, lastName);
	}

	@Override
	public boolean canCheckIn(Calendar time) {
		return true;
	}

	@Override
	public MembershipType getMembershipType() {
		return MembershipType.ANYTIME;
	}
}
