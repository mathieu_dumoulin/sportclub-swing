package glo2004.guidemo.model;

import java.util.Calendar;

public interface Member extends Comparable<Member> {
	public boolean canCheckIn(Calendar time);

	public String getFirstName();

	public void setFirstName(String firstName);

	public String getLastName();

	public void setLastName(String lastName);

	public int getID();

	public MembershipType getMembershipType();
}
