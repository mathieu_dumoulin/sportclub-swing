package glo2004.guidemo.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public abstract class AbstractMember implements Member {
	String firstName;
	String lastName;
	int id;

	AbstractMember(int id, String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public int compareTo(Member other) {
		return ((Integer) id).compareTo(other.getID());
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(firstName).append(lastName)
				.toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != getClass()) {
			return false;
		}
		AbstractMember rhs = (AbstractMember) obj;
		return new EqualsBuilder().appendSuper(super.equals(obj)).append(id, rhs.id)
				.append(firstName, rhs.firstName).append(lastName, rhs.lastName).isEquals();
	}

	@Override
	public String toString() {
		return lastName + ", " + firstName + " (" + id + ")";
	}
}
