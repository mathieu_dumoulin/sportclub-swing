package glo2004.guidemo.model;

public enum MembershipType {
	DAY, NIGHT, ANYTIME, WEEKEND
}
