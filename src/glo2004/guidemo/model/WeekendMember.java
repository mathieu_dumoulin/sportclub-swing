package glo2004.guidemo.model;

import java.util.Calendar;

public class WeekendMember extends AbstractMember {
	public WeekendMember(int id, String firstName, String lastName) {
		super(id, firstName, lastName);
	}

	@Override
	public boolean canCheckIn(Calendar time) {
		int dayOfWeek = time.get(Calendar.DAY_OF_WEEK);
		if (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY) {
			return true;
		}
		return false;
	}

	@Override
	public MembershipType getMembershipType() {
		return MembershipType.WEEKEND;
	}
}
