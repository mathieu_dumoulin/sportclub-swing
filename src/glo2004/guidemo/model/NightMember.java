package glo2004.guidemo.model;

import java.util.Calendar;

public class NightMember extends AbstractMember {
	private static final int NIGHT_START_HOUR = 20;

	public NightMember(int id, String firstName, String lastName) {
		super(id, firstName, lastName);
	}

	@Override
	public boolean canCheckIn(Calendar time) {
		return time.get(Calendar.HOUR_OF_DAY) >= NIGHT_START_HOUR ? true : false;
	}

	@Override
	public MembershipType getMembershipType() {
		return MembershipType.NIGHT;
	}
}
