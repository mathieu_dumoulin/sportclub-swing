package glo2004.guidemo.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SportClub {
	private static MemberFactory memberFactory = new MemberFactory();
	Map<Integer, Member> membersMap = new HashMap<>();

	public List<Member> getMembers() {
		return Collections.list(Collections.enumeration(membersMap.values()));
	}

	public static MemberFactory getMemberFactory() {
		return memberFactory;
	}

	public void addMember(String firstName, String lastName, MembershipType type) {
		Member newMember = memberFactory.create(firstName, lastName, type);
		membersMap.put(newMember.getID(), newMember);
	}

	public void updateMemberInfo(int id, String firstName, String lastName) {
	}

	public void updateMember(int id, String firstName, String lastName, MembershipType newType) {
		membersMap.remove(id);
		membersMap.put(id, memberFactory.createWithID(id, firstName, lastName, newType));
	}

	public Member getMember(int id) {
		return membersMap.get(id);
	}
}
