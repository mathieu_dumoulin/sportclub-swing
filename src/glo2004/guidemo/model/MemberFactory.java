package glo2004.guidemo.model;

public class MemberFactory {
	static int nextMemberID = 1000;

	public Member create(String firstName, String lastName, MembershipType type) {
		return createWithID(nextMemberID++, firstName, lastName, type);
	}

	public Member createWithID(int id, String firstName, String lastName, MembershipType type) {
		Member newMember = null;
		switch (type) {
		case ANYTIME:
			newMember = new AnytimeMember(id, firstName, lastName);
			break;
		case DAY:
			newMember = new DayMember(id, firstName, lastName);
			break;
		case NIGHT:
			newMember = new NightMember(id, firstName, lastName);
			break;
		case WEEKEND:
			newMember = new WeekendMember(id, firstName, lastName);
			break;
		default:
			throw new IllegalArgumentException("unknown membership type: " + type);
		}

		return newMember;
	}

}
