package glo2004.guidemo.model;

import java.util.Calendar;

public class DayMember extends AbstractMember {
	private static int DAY_END_HOUR = 19;

	public DayMember(int id, String firstName, String lastName) {
		super(id, firstName, lastName);
	}

	@Override
	public boolean canCheckIn(Calendar time) {
		return time.get(Calendar.HOUR_OF_DAY) <= DAY_END_HOUR ? true : false;
	}

	@Override
	public MembershipType getMembershipType() {
		return MembershipType.DAY;
	}
}
