package glo2004.guidemo.view;

import glo2004.guidemo.controller.Controller;
import glo2004.guidemo.model.Member;
import glo2004.guidemo.model.MembershipType;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collections;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

@SuppressWarnings("serial")
public class MainPanel extends JFrame implements ActionListener, ListSelectionListener {

	// private static final String IMAGES_ADD_USER_PNG_URL =
	// "images/add-user.png";
	private static final String UPDATE_MEMBER = "Update Member";
	private static final String TYPE_CHANGED = "Type Changed";
	private static final String ADD_MEMBER = "Add Member";

	private JToolBar toolBar;
	private JList<Member> memberList;
	// private final JPanel rightContentPanel;
	private JPanel memberDetailsPanel;
	private JTextField firstNameTextField;
	private JTextField lastNameTextField;
	private JComboBox<MembershipType> memberTypeCombo;
	private JButton updateMemberButton;
	private final JLabel statusBar;

	private Controller controller;

	public MainPanel() {
		super("SportClub Swing MVC GUI demo");
		setLayout(new BorderLayout());
		setPreferredSize(new Dimension(800, 400));
		setLocation(600, 200);

		toolBar = createToolBar();
		add(toolBar, BorderLayout.PAGE_START);

		statusBar = new JLabel("Welcome to SportClub GUI Demo!");
		add(statusBar, BorderLayout.PAGE_END);

		JScrollPane leftScrollPane = createMemberListPane();
		add(leftScrollPane, BorderLayout.WEST);

		JPanel contentPanel = createContentPanel();
		add(contentPanel, BorderLayout.CENTER);

		pack();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private JPanel createContentPanel() {
		JPanel contentPanel = new JPanel(new BorderLayout());
		memberDetailsPanel = createMemberDetailsPanel();
		contentPanel.add(memberDetailsPanel, BorderLayout.CENTER);

		JPanel buttonsPanel = createButtonsPanel();
		contentPanel.add(buttonsPanel, BorderLayout.SOUTH);

		return contentPanel;
	}

	private JToolBar createToolBar() {
		toolBar = new JToolBar("Still draggable");
		toolBar.setPreferredSize(new Dimension(600, 40));

		JButton button = new JButton();
		button.setActionCommand(ADD_MEMBER);
		button.setToolTipText(ADD_MEMBER);
		button.addActionListener(this);
		button.setIcon(new ImageIcon("images/add-participant.png", ADD_MEMBER));
		toolBar.add(button);

		return toolBar;
	}

	private JPanel createButtonsPanel() {
		JPanel buttonsPanel = new JPanel(new FlowLayout());

		updateMemberButton = new JButton(UPDATE_MEMBER);
		updateMemberButton.setActionCommand(UPDATE_MEMBER);
		updateMemberButton.addActionListener(this);
		updateMemberButton.setEnabled(false);
		buttonsPanel.add(updateMemberButton);
		return buttonsPanel;
	}

	private JPanel createMemberDetailsPanel() {
		JPanel memberDetailsPanel = new JPanel(new GridLayout(3, 2, 10, 10));

		firstNameTextField = new JTextField(20);
		firstNameTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {
				setUpdateButton();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				setUpdateButton();

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				setUpdateButton();
			}

			private void setUpdateButton() {
				if (!memberList.getSelectedValue().getFirstName()
						.equals(firstNameTextField.getText())) {
					updateMemberButton.setEnabled(true);
				} else {
					updateMemberButton.setEnabled(false);
				}
			}

		});
		lastNameTextField = new JTextField(20);
		lastNameTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent e) {
				setUpdateButton();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				setUpdateButton();

			}

			@Override
			public void removeUpdate(DocumentEvent e) {
				setUpdateButton();
			}

			private void setUpdateButton() {
				if (!memberList.getSelectedValue().getLastName()
						.equals(lastNameTextField.getText())) {
					updateMemberButton.setEnabled(true);
				} else {
					updateMemberButton.setEnabled(false);
				}
			}

		});
		memberTypeCombo = new JComboBox<>(MembershipType.values());
		memberTypeCombo.setActionCommand(TYPE_CHANGED);
		memberTypeCombo.addActionListener(this);

		JLabel firstNameLabel = new JLabel("First Name: ");
		firstNameLabel.setLabelFor(firstNameTextField);
		JLabel lastNameLabel = new JLabel("Last Name: ");
		lastNameLabel.setLabelFor(lastNameLabel);
		JLabel membershipType = new JLabel("Member Type: ");
		membershipType.setLabelFor(memberTypeCombo);

		memberDetailsPanel.add(firstNameLabel);
		memberDetailsPanel.add(firstNameTextField);
		memberDetailsPanel.add(lastNameLabel);
		memberDetailsPanel.add(lastNameTextField);
		memberDetailsPanel.add(membershipType);
		memberDetailsPanel.add(memberTypeCombo);

		memberDetailsPanel.setPreferredSize(new Dimension(400, 400));

		return memberDetailsPanel;
	}

	private JScrollPane createMemberListPane() {
		memberList = new JList<>(new DefaultListModel<Member>());
		memberList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		memberList.addListSelectionListener(this);
		memberList.setPreferredSize(new Dimension(200, 400));
		memberList.setVisibleRowCount(5);
		JScrollPane listScrollPane = new JScrollPane(memberList);
		return listScrollPane;
	}

	public void registerController(Controller controller) {
		this.controller = controller;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getActionCommand() == TYPE_CHANGED) {
			MembershipType selectedType = (MembershipType) memberTypeCombo.getSelectedItem();
			if (memberList.getSelectedValue().getMembershipType() != selectedType) {
				updateMemberButton.setEnabled(true);
			} else {
				updateMemberButton.setEnabled(false);
			}
		}
		if (e.getActionCommand() == UPDATE_MEMBER) {
			MembershipType selected = (MembershipType) memberTypeCombo.getSelectedItem();
			controller.updateMember(memberList.getSelectedValue().getID(),
					firstNameTextField.getText(), lastNameTextField.getText(), selected);
		}

		if (e.getActionCommand() == ADD_MEMBER) {
			controller.processAddMember();
		}
	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		Member selectedMember = memberList.getSelectedValue();
		if (selectedMember != null) {
			firstNameTextField.setText(selectedMember.getFirstName());
			lastNameTextField.setText(selectedMember.getLastName());
			memberTypeCombo.setSelectedItem(selectedMember.getMembershipType());

			boolean enableButton = controller.wasUpdated(selectedMember);
			updateMemberButton.setEnabled(enableButton);
		}
	}

	public void updateListContents() {
		if (controller == null)
			return;

		DefaultListModel<Member> defaultModel = new DefaultListModel<Member>();
		List<Member> members = controller.getMembers();
		Collections.sort(members);
		for (Member member : members) {
			defaultModel.addElement(member);
		}
		memberList.setModel(defaultModel);
		memberList.setSelectedIndex(0);
		updateMemberButton.setEnabled(false);
	}

	public void selectMember(Member toSelect) {
		ListModel<Member> model = memberList.getModel();
		for (int i = 0; i < model.getSize(); i++) {
			if (model.getElementAt(i).equals(toSelect)) {
				memberList.setSelectedIndex(i);
				return;
			}
		}
	}

	public void selectNewMember() {
		memberList.setSelectedIndex(memberList.getModel().getSize() - 1);
		updateMemberButton.setEnabled(true);
	}

	public void setStatusBar(String message) {
		statusBar.setText(message);
	}
}