package glo2004.guidemo;

import glo2004.guidemo.controller.Controller;
import glo2004.guidemo.model.MembershipType;
import glo2004.guidemo.model.SportClub;
import glo2004.guidemo.view.MainPanel;

public class Main {

	public static SportClub createModelWithData() {
		SportClub model = new SportClub();
		model.addMember("Jacques", "Fontaine", MembershipType.DAY);
		model.addMember("Laurent", "Meilleur", MembershipType.NIGHT);
		model.addMember("Josée", "Benoît", MembershipType.ANYTIME);
		model.addMember("Martine", "Jeunet", MembershipType.WEEKEND);
		model.addMember("Nicolas", "Murray", MembershipType.DAY);
		return model;
	}

	public static void main(String[] args) {
		SportClub model = createModelWithData();
		MainPanel view = new MainPanel();
		Controller controller = new Controller(model, view);
		view.registerController(controller);
		controller.updateViewToMatchModel();
	}
}